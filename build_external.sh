#!/bin/bash

# This script is used as a one touch build system for the Dune course 2015
# held at IWR, University of Heidelberg. For more general documentation on
# how to configure and install Dune, check www.dune-project.org

# You can configure this script by setting the following environment variables:
# See brackets for defaults.
#
# INSTALL_HOME the directory to install external packages in ($HOME/external)
# F77 the fortran compiler (gfortran)
# CC the C compiler (gcc)
# MPICC the MPI compiler (mpicc)
# CXX the C++ compiler (g++)
# CXXFLAGS the standard C++ flags for external libraries ("-O3 -DNEBUG")
# CFLAGS the standard C flags for external libraries (copy CXXFLAGS)
# MAKE_FLAGS flags to be given to make during the build process ("-j2")
#
# You can disable some parts of this script by setting the variables NOBUILD_<part>,
# where part is either UG or DUNE (which disables the call to the build_exercise.sh script).


# set the proper defaults for variables which may be set from the outside.
set -x
set -e
ROOT=$(pwd)
if [ ! "$INSTALL_HOME" ]; then
  INSTALL_HOME=$ROOT/external
fi
if [ ! "$F77" ]; then
  F77=gfortran
fi
if [ ! "$CC" ]; then
CC=gcc
fi
if [ ! "$MPICC" ]; then
MPICC=mpicc
fi
if [ ! "$CXX" ]; then
CXX=g++
fi
if [ ! "$CXXFLAGS" ]; then
CXXFLAGS="-O3 -DNDEBUG"
fi
CFLAGS="$CXXFLAGS"
if [ ! "$MAKE_FLAGS" ]; then
MAKE_FLAGS="-j2"
fi

if [ ! $NOBUILD_GRIDS ]; then

## compile and install UG 3.11
if [ ! $NOBUILD_UG ] ; then
pushd $INSTALL_HOME
pushd tarballs
rm -rf ug-3.12.1
tar xzf ug-3.12.1.tar.gz
pushd ug-3.12.1
( ./configure MPICC=$MPICC CC=$CXX --prefix=$INSTALL_HOME/ug CXXFLAGS="$CXXFLAGS" --enable-parallel --enable-dune && make $MAKE_FLAGS && make install ) || exit $?
popd
rm -rf ug-3.12.1
popd
popd
fi

fi

CC=$(which $CC)
CXX=$(which $CXX)
F77=$(which $F77)

# generate an opts file with release flags
echo "CMAKE_FLAGS=\"
-DUG_ROOT=$INSTALL_HOME/ug
-DCMAKE_C_COMPILER='$CC'
-DCMAKE_CXX_COMPILER='$CXX'
-DCMAKE_Fortran_COMPILER='$F77'
-DCMAKE_CXX_FLAGS_RELEASE='-O3 -DNDEBUG -g0 -Wno-deprecated-declarations -funroll-loops'
-DCMAKE_BUILD_TYPE=Release
-DDUNE_SYMLINK_TO_SOURCE_TREE=1
\"" > release.opts

# generate an opts file with debug flags
echo "CMAKE_FLAGS=\"
-DUG_ROOT=$INSTALL_HOME/ug
-DCMAKE_C_COMPILER='$CC'
-DCMAKE_CXX_COMPILER='$CXX'
-DCMAKE_Fortran_COMPILER='$F77'
-DCMAKE_CXX_FLAGS_DEBUG='-O0 -ggdb -Wall'
-DCMAKE_BUILD_TYPE=Debug
-DDUNE_SYMLINK_TO_SOURCE_TREE=1
\"" > debug.opts

# now download all the dune modules and all exercise modules
git submodule init
git submodule update

if [ ! $NOBUILD_DUNE ] ; then
./build_exercise.sh
fi
